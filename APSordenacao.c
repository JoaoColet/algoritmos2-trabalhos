#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void gerarvet(int *v, int tam)
{
    int x, i;
    for(i=0; i<tam; i++)
    {
        v[i]=rand()%10000;
    }
}

void copiavet(int *v, int *vo, int tam)
{
    int i;
    for(i=0; i<tam; i++)
    {
        vo[i]=v[i];
    }
}

void quick(int *v, int p, int r)
{
    if(p<r)
    {
    int j=separa(v, p, r);
    quick(v, p, j-1);
    quick(v, j+1, r);
    }
}

int separa(int *v,  int p, int r)
{
    int c=v[p], i=p+1, j=r, t;
    while(i<=j)
    {
        if(v[i]<=c)
            i++;
        else if(c<v[j])
            j--;
        else
        {
            t=v[i];
            v[i]=v[j];
            v[j]=t;
            i++;
            j--;
        }
    }
    v[p]=v[j];
    v[j]=c;
    return j;
}

void bolha(int *v, int n)
{
  int i, j, aux;

  for(i=0; i<n; i++)
  {
     for(j=i+1; j<n; j++)
     {
        if(v[j] < v[i])
        {
            aux=v[i];
            v[i]=v[j];
            v[j]=aux;
        }
     }
  }
}

void marge(int *v, int p, int r)
{
    if(p<r)
    {
        int q=(r+p)/2;
        marge(v, p, q);
        marge(v, q+1, r);
        intercala(v, p, q, r);
    }
}

void intercala(int *v, int p, int q, int r)
{
    int i=p, j=q+1, k=0, *w;
    w=malloc((r-p+1)*sizeof(int));

    while(i<=q && j<=r)
    {
        if(v[i]<v[j])
        {
            w[k++]=v[i++];
        }
        else
        {
            w[k++]=v[j++];
        }
    }
    while(i<=q)
        w[k++]=v[i++];
    while(j<=r)
        w[k++]=v[j++];

    for(i=p, j=0; i<=r;i++, j++)
    {
        v[i]=w[j];
    }
    free(w);
}

int main(void)
{
    FILE *saida=fopen("saida.txt", "w");
    fprintf(saida, "Tempo de execu��o para algoritimos de ordena��o em segundos.\n");
    int tam;
    while(printf("Tamanho (0 para sair) ->"), scanf("%d", &tam), tam!=0)
    {
    int i, v[tam], vo[tam];
    gerarvet(v, tam);
    float tempo;
    clock_t ti, tf;

    copiavet(v, vo, tam);
    ti=clock();
    bolha(vo, tam);
    tf=clock();
    tempo=((float)(tf-ti))/CLOCKS_PER_SEC;
    fprintf(saida, "Para um vetor de %d elementos -> Bubble = %.4f // ",tam,  tempo);

    copiavet(v, vo, tam);
    ti=clock();
    quick(vo, 0, tam);
    tf=clock();
    tempo=((float)(tf-ti))/CLOCKS_PER_SEC;
    fprintf(saida,"Quick = %.4f // ", tempo);

    copiavet(v, vo, tam);
    ti=clock();
    marge(vo, 0, tam-1);
    tf=clock();
    tempo=((float)(tf-ti))/CLOCKS_PER_SEC;
    fprintf(saida, "Marge = %.4f\n", tempo);
    }
return 0;
}
