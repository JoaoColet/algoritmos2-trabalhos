#include <stdio.h>
#include <stdlib.h>
#define tipoconteudo int
#define TRUE 1
#define FALSE 0;

typedef struct aresta
{
    int peso;
    tipoconteudo conteudo;
    struct aresta *prox;
}aresta;

typedef struct celula
{
    tipoconteudo conteudo;
    struct celula *prox;
    struct aresta *prx;
    int grau;
}celula;

typedef struct lista_st
{
    celula *inicio;
    int cont;
}lista_st;

void inicializaLista(lista_st *l)
{
    l->inicio = NULL;
    l->cont = 0;
}

int inserearesta(lista_st *l, tipoconteudo conteudo, tipoconteudo arst, int p)
{
    aresta *novo = (aresta*)malloc(sizeof(aresta));
    if(novo==NULL)
        return FALSE;
    celula *aux = l->inicio;
    novo->peso=p;
    while(aux!=NULL)
    {
        if(aux->conteudo==conteudo)
        {
            if(aux->prx==NULL)
            {
                aux->prx=novo;
                novo -> prox = NULL;
                novo -> conteudo = arst;
                aux->grau++;
                return TRUE;
            }
            aresta *ultimo=aux->prx;
            while(ultimo!=NULL)
            {
                if(ultimo->conteudo==arst)
                {
                    free(novo);
                    return FALSE;
                }
                if(ultimo->prox==NULL)
                {
                    aux->grau++;
                    ultimo->prox=novo;
                    novo->prox=NULL;
                    novo->conteudo=arst;
                    return TRUE;
                }
                ultimo=ultimo->prox;
            }
        }
        aux=aux->prox;
    }
    free(novo);
    return FALSE;
}

int insereinicio(lista_st *l, tipoconteudo conteudo)
{
    celula *novo = (celula*)malloc(sizeof(celula));
    if(novo==NULL)
        return FALSE;

    novo->conteudo=conteudo;
    novo->prox = l->inicio;
    l->inicio = novo;
    l->cont++;
    novo->prx=NULL;
    novo->grau=0;

    return TRUE;
}

int inserefim(lista_st *l, tipoconteudo conteudo)
{
    if(l->inicio==NULL)
        insereinicio(l,conteudo);
    else
    {
        celula *novo = (celula*)malloc(sizeof(celula));
        if(novo==NULL)
            return FALSE;

        celula *ultimo = l->inicio;
        while(ultimo!=NULL)
        {
            if(ultimo->conteudo==conteudo)
            {
                free(novo);
                return FALSE;
            }
            if(ultimo->prox==NULL)
            {
                novo->prox=NULL;
                novo->prx=NULL;
                novo->conteudo=conteudo;
                ultimo->prox = novo;
                l->cont++;
                novo->grau=0;
                return TRUE;
            }
            ultimo = ultimo->prox;
        }
    }
}

void removeumaaresta(lista_st *l, tipoconteudo conteudo, int x)
{
    celula *aux=l->inicio;
    while(aux!=NULL)
    {
        if(aux->conteudo==conteudo && aux->prx!=NULL)
        {
            aresta *rem1, *rem2;
            rem1=aux->prx;
            rem2=rem1->prox;
            if(rem2==NULL && rem1->conteudo==x)
            {
                aux->grau--;
                free(rem1);
                aux->prx=NULL;
                return;
            }
            else
            {
                if(rem1->conteudo==x)
                {
                    aux->grau--;
                    aux->prx=rem2;
                    free(rem1);
                    return;
                }
                while(rem2!=NULL)
                {
                    if(rem2->conteudo==x)
                    {
                        aux->grau--;
                        rem1->prox=rem2->prox;
                        free(rem2);
                        return;
                    }
                    rem1=rem2;
                    rem2=rem2->prox;
                }
            }
        }
        aux=aux->prox;
    }
}

int removetodasaresta(lista_st *l, int k)
{
    int marcador=0;
    celula *auxV=l->inicio;
    while(auxV!=NULL)
    {
        if(auxV->conteudo==k)
        {
            if(auxV->prx==NULL)
                return marcador;
            else
            {
                aresta *aux1, *aux2;
                aux1=auxV->prx;
                if(aux1->prox==NULL)
                {
                    auxV->prx=NULL;
                    free(aux1);
                    return marcador;
                }
                else
                {
                    aux2=aux1->prox;
                    while(aux2!=NULL)
                    {
                        free(aux1);
                        aux1=aux2;
                        aux2=aux2->prox;
                    }
                    auxV->prx=NULL;
                    free(aux1);
                    return marcador;
                }
            }
        }
        auxV=auxV->prox;
        marcador++;
    }
}

void removearestas(lista_st *l, int k)
{
    int x;
    celula *aux=l->inicio;
    while(aux!=NULL)
    {
        removeumaaresta(l, aux->conteudo, k);
        aux=aux->prox;
    }
}

void removevert(lista_st *l,int k)
{
    int cont=1;

    if(k==0)
    {
        celula *ret=l->inicio;
        l->inicio=l->inicio->prox;
        free(ret);
        l->cont--;
    }
    else
    {
    celula *aux1, *aux2;
    aux1=l->inicio;
    aux2=aux1->prox;

    while(cont!=k)
    {
        aux1=aux2;
        aux2=aux2->prox;
        cont++;
    }
    l->cont--;
    aux1->prox=aux2->prox;
    free(aux2);
    }
}

void imprimelista(lista_st *l)
{
    celula *aux=l->inicio;
    aresta *aux2;
    printf("Vertice -> Aresta [Peso]\n");
    while(aux!=NULL)
    {
        printf("%d ->",aux->conteudo);
        if(aux->prx!=NULL)
        {
            aux2=aux->prx;
            while(aux2 != NULL)
            {
                printf(" %d [%d];", aux2->conteudo, aux2->peso);
                aux2=aux2->prox;
            }
        }
        printf("\n");
        aux=aux->prox;
    }
}

int verifica(lista_st *l, int a, int b)
{
    int v=0;
    celula *aux=l->inicio;
    while(aux!=NULL)
    {
        if(aux->conteudo==a || aux->conteudo==b)
            v++;
        aux=aux->prox;
    }
    if(v==2)
        return TRUE;
    else
        return FALSE;
}

int informagrau(lista_st *l, int x)
{
    celula *aux=l->inicio;
    while(aux!=NULL)
    {
        if(aux->conteudo==x)
            return aux->grau;
        aux=aux->prox;
    }
    return -1;
}

void matriz(lista_st *l)
{
    int i, j, vet[l->cont][2];
    celula *aux=l->inicio;
    for(i=0; i<l->cont; i++)
    {
        vet[i][0]=aux->conteudo;
        aux=aux->prox;
    }
    for(i=0; i<l->cont; i++)
    {
        vet[i][1]=0;
    }
    printf(" \\ %d",vet[0][0]);
    for(i=1; i<l->cont; i++)
        printf(" %d",vet[i][0]);
    printf("\n");

    aux=l->inicio;
    aresta *perc;
    for(i=0; i<l->cont; i++)
    {
        if(vet[i][0]>9)
            printf("%d ",vet[i][0]);
        else
            printf(" %d ",vet[i][0]);
        perc=aux->prx;
        while(perc!=NULL)
        {
            for(j=0;j<l->cont;j++)
            {
                if(perc->conteudo==vet[j][0])
                    vet[j][1]=1;
            }
        perc=perc->prox;
        }
        for(j=0;j<l->cont;j++)
        {
            printf("%d ",vet[j][1]);
            vet[j][1]=0;
        }
        printf("\n");
        aux=aux->prox;
    }
    printf("\n");
}

int conexo(lista_st *l)
{
    int t=l->cont, i, z, x, pause, j, k, minimo;
    int m[t][4];
    celula *aux=l->inicio;
    aresta *perc;
    x=aux->conteudo;
    for(i=0; i<t;i++)
    {
        if(aux->conteudo==x)
        {
        m[i][0]=aux->conteudo; // vertice
        m[i][1]=-1; // pai
        m[i][2]=0; // key
        m[i][3]=0;
        }
        else
        {
        m[i][0]=aux->conteudo; // vertice
        m[i][1]=-1; //pai
        m[i][2]=-1; // key
        m[i][3]=0;
        }
        aux=aux->prox;
    }
    k=0;
    while(k<t)
    {
        k++;
        minimo=0;
        for(i=0;i<t;i++)
        {
            if(m[i][2]!=-1 && minimo==0 && m[i][3]==0)
            {
                minimo=m[i][2];
                z=m[i][0];
            }
            else if(m[i][2]!=-1 && m[i][2]<minimo && m[i][3]==0)
            {
                minimo=m[i][2];
                z=m[i][0];
            }
        }

        for(i=0;i<t;i++)
        {
            if(m[i][0]==z)
            {
                m[i][3]=1;
                i=t;
            }
        }

        aux=l->inicio;
        pause=0;
        while(pause==0 && aux!=NULL)
        {
            if(aux->conteudo==z)
            {
                perc=aux->prx;
                while(perc!=NULL)
                {
                    for(j=0;j<t;j++)
                    {
                        if(perc->conteudo==m[j][0] && m[j][2]==-1)
                        {
                            m[j][1]=aux->conteudo;
                            m[j][2]=perc->peso;
                            j=t;
                        }
                        else if(perc->conteudo==m[j][0] && m[j][2]>perc->peso && m[j][3]==0)
                        {
                                m[j][1]=aux->conteudo;
                                m[j][2]=perc->peso;
                        }
                    }
                    perc=perc->prox;
                }
                pause=1;
            }
            aux=aux->prox;
        }
    }
    for(i=0;i<t;i++)
    {
        if(m[i][2]==-1)
            return 0;
    }
    return 1;
}

void buscalargura(lista_st *l, int x)
{
    int i, t=l->cont, fila=0, j, cont=0;
    int m[t][4];
    int v[t]; // fila
    celula *aux=l->inicio;
    aresta *perc;

    for(i=0;i<t;i++)
    {
        v[i]=-1;
    }
    for(i=0; i<t; i++)
    {
        if(aux->conteudo==x)
        {
            m[i][0]=-1; // pai
            m[i][1]=0;  // distancia
            m[i][2]=1;  //cor
            m[i][3]=aux->conteudo;
            v[fila]=i;
            fila++;
        }
        else
        {
            m[i][0]=-1;
            m[i][1]=-1;
            m[i][2]=0;
            m[i][3]=aux->conteudo;
        }
        aux=aux->prox;
    }

    while(cont<t && v[cont]!=-1)
    {
        j=v[cont];
        cont++;
        x=m[j][3];
        aux=l->inicio;
        while(aux!=NULL)
        {
            if(aux->conteudo==x)
            {
                perc=aux->prx;
                while(perc!=NULL)
                {
                    for(i=0;i<t;i++)
                    {

                        if(perc->conteudo==m[i][3] && m[i][2]==0)
                        {
                            v[fila]=i;
                            fila++;
                            m[i][0]=x;
                            m[i][1]=(m[j][1])+1;
                            m[i][2]=1;
                            i=t;
                        }
                    }
                    perc=perc->prox;
                }
                m[j][2]=2;
            }
            aux=aux->prox;
        }
    }

    printf("Vertice // Pai do vertice // Distancia da origem\n");
    for(i=0;i<t;i++)
    {
        printf("   %d   //   %d   //   %d   \n", m[i][3], m[i][0], m[i][1]);
    }
    printf("\n");
}

void buscaprofundidade(lista_st *l, int x)
{
    int i, t=l->cont, pilha=0, y, j, tempo=2, z=x;
    int m[t][4];
    int v[t]; // pilha
    celula *aux=l->inicio;
    aresta *perc;

    for(i=0; i<t; i++)
    {
        if(aux->conteudo==x)
        {
            m[i][0]=aux->conteudo; // vertice
            m[i][1]=1;  // descoberta
            m[i][2]=0;  //finalização
            m[i][3]=-1; // pai
            v[pilha]=aux->conteudo;
            pilha++;
        }
        else
        {
            m[i][0]=aux->conteudo;
            m[i][1]=0;
            m[i][2]=0;
            m[i][3]=-1;
            m[i][4]=0;
        }
        aux=aux->prox;
    }

    while(pilha>0)
    {
        pilha--;
        z=v[pilha];
        aux=l->inicio;
        while(aux->conteudo!=z)
        {
            aux=aux->prox;
        }
        perc=aux->prx;
        y=0;
        while(perc!=NULL && y==0)
        {
            int k=perc->conteudo;
            for(i=0;i<t;i++)
            {
                if(k==m[i][0] && m[i][1]==0)
                {
                    m[i][1]=tempo;
                    m[i][3]=z;
                    v[pilha]=k;
                    pilha++;
                    tempo++;
                    y++;
                    i=t;

                }
            }
            perc=perc->prox;
        }
    }
    z=(tempo-1)*2;
    j=tempo-1;
    while(tempo<=z)
    {
        for(i=0;i<t;i++)
        {
            if(m[i][1]==j)
            {
                m[i][2]=tempo;
                tempo++;
                j--;
                i=t;
            }
        }
    }

    printf("Vertice // Descoberta // Finalizacao // Pai\n");
    for(i=0;i<t;i++)
    {
        printf("    %d    //    %d    //    %d    //    %d    \n",m[i][0], m[i][1], m[i][2], m[i][3]);
    }
    printf("\n");
}

void encerrar(lista_st *l)
{
    celula *auxV=l->inicio;
    celula *auxV2;
    while(auxV!=NULL)
    {
        if(auxV->prx==NULL)
        {
            auxV2=auxV->prox;
            free(auxV);
            auxV=auxV2;
        }
        else
        {
            aresta *aux1, *aux2;
            aux1=auxV->prx;
            if(aux1->prox==NULL)
            {
                auxV->prx=NULL;
                free(aux1);
            }
            else
            {
                aux2=aux1->prox;
                while(aux2!=NULL)
                {
                    free(aux1);
                    aux1=aux2;
                    aux2=aux2->prox;
                }
                auxV->prx=NULL;
                free(aux1);
            }
            auxV2=auxV->prox;
            free(auxV);
            auxV=auxV2;
        }
    }
    l->inicio=NULL;
    l->cont=0;
}


void caminhominimoprim(lista_st *l, int x)
{
    int t=l->cont, i, z, fila=1, pause, j, k, y, minimo;
    int m[t][4];
    celula *aux=l->inicio;
    aresta *perc;
    for(i=0; i<t;i++)
    {
        if(aux->conteudo==x)
        {
        m[i][0]=aux->conteudo; // vertice
        m[i][1]=-1; // pai
        m[i][2]=0; // key
        m[i][3]=0;
        }
        else
        {
        m[i][0]=aux->conteudo; // vertice
        m[i][1]=-1; //pai
        m[i][2]=-1; // key
        m[i][3]=0;
        }
        aux=aux->prox;
    }
    k=0;
    while(k<t)
    {
        k++;
        minimo=0;
        for(i=0;i<t;i++)
        {
            if(m[i][2]!=-1 && minimo==0 && m[i][3]==0)
            {
                minimo=m[i][2];
                z=m[i][0];
            }
            else if(m[i][2]!=-1 && m[i][2]<minimo && m[i][3]==0)
            {
                minimo=m[i][2];
                z=m[i][0];
            }
        }

        for(i=0;i<t;i++)
        {
            if(m[i][0]==z)
            {
                m[i][3]=1;
                i=t;
            }
        }

        aux=l->inicio;
        pause=0;
        while(pause==0 && aux!=NULL)
        {
            if(aux->conteudo==z)
            {
                perc=aux->prx;
                while(perc!=NULL)
                {
                    for(j=0;j<t;j++)
                    {
                        if(perc->conteudo==m[j][0] && m[j][2]==-1)
                        {
                            m[j][1]=aux->conteudo;
                            m[j][2]=perc->peso;
                            j=t;
                        }
                        else if(perc->conteudo==m[j][0] && m[j][2]>perc->peso && m[j][3]==0)
                        {
                                m[j][1]=aux->conteudo;
                                m[j][2]=perc->peso;
                        }
                    }
                    perc=perc->prox;
                }
                pause=1;
            }
            aux=aux->prox;
        }
    }
    printf("Vertice // Pai // key\n");
    for(i=0;i<t;i++)
    {
        printf("  %d  //  %d  //  %d\n", m[i][0],m[i][1], m[i][2]);
    }
    printf("\n");
}

void caminhominimodijkstra(lista_st *l, int x)
{
    int t=l->cont, i, k, minimo, z, pause, j;
    int m[t][4];
    celula *aux=l->inicio;
    aresta *perc;
    for(i=0; i<t;i++)
    {
        if(aux->conteudo==x)
        {
        m[i][0]=aux->conteudo; // vertice
        m[i][1]=-1; // pai
        m[i][2]=0; // key
        m[i][3]=0;
        }
        else
        {
        m[i][0]=aux->conteudo; // vertice
        m[i][1]=-1; //pai
        m[i][2]=-1; // key
        m[i][3]=0;
        }
        aux=aux->prox;
    }
    k=0;
    while(k<t)
    {
        k++;
        minimo=-1;
        for(i=0;i<t;i++)
        {
            if(m[i][2]!=-1 && minimo==-1 && m[i][3]==0)
            {
                minimo=m[i][2];
                z=m[i][0];
            }
            else if(m[i][2]!=-1 && m[i][2]<minimo && m[i][3]==0)
            {
                minimo=m[i][2];
                z=m[i][0];
            }
        }

        for(i=0;i<t;i++)
        {
            if(m[i][0]==z)
            {
                m[i][3]=1;
                i=t;
            }
        }

        aux=l->inicio;
        pause=0;
        while(pause==0 && aux!=NULL)
        {
            if(aux->conteudo==z)
            {
                perc=aux->prx;
                while(perc!=NULL)
                {
                    for(j=0; j<t; j++)
                    {
                        if(perc->conteudo==m[j][0] && m[j][3]==0)
                        {
                            if(m[j][2]==-1)
                            {
                                m[j][1]=aux->conteudo;
                                m[j][2]=minimo+perc->peso;
                            }
                            else if(m[j][2]>(minimo+perc->peso))
                            {
                                m[j][1]=aux->conteudo;
                                m[j][2]=minimo+perc->peso;
                            }
                        }
                    }
                    perc=perc->prox;
                }
            }
            aux=aux->prox;
        }
    }
    printf("Vertice // Pai // distancia\n");
    for(i=0;i<t;i++)
    {
        printf("  %d  //  %d  //  %d\n", m[i][0],m[i][1], m[i][2]);
    }
    printf("\n");
}

int tamanho(lista_st *l)
{
    return l->cont;
}

int existe(lista_st *l, int x)
{
    celula *aux=l->inicio;
    while(aux!=NULL)
    {
        if(aux->conteudo==x)
            return 1;
        aux=aux->prox;
    }
    return 0;
}

int main(void)
{
    lista_st l;
    int i, x, comando=0, a1, a2, p, y, j;
    inicializaLista(&l);
    inserefim(&l, 1); inserefim(&l, 2); inserefim(&l, 3); inserefim(&l, 4); inserefim(&l, 5); inserefim(&l, 6); inserefim(&l, 7); inserefim(&l, 8); inserefim(&l, 9); inserefim(&l, 10);
    inserearesta(&l, 1, 2, 7); inserearesta(&l, 1, 4, 9); inserearesta(&l, 1, 5, 10); inserearesta(&l, 1, 6, 8); inserearesta(&l, 1, 8, 9); inserearesta(&l, 1, 10, 8); inserearesta(&l, 2, 1, 7);
    inserearesta(&l, 2, 3, 4); inserearesta(&l, 2, 4, 5); inserearesta(&l, 2, 9, 9); inserearesta(&l, 2, 10, 9); inserearesta(&l, 3, 2, 4); inserearesta(&l, 3, 4, 1); inserearesta(&l, 3, 5, 4);
    inserearesta(&l, 4, 1, 9); inserearesta(&l, 4, 2, 5); inserearesta(&l, 4, 3, 1); inserearesta(&l, 4, 5, 3); inserearesta(&l, 5, 1, 10); inserearesta(&l, 5, 3, 4); inserearesta(&l, 5, 4, 3);
    inserearesta(&l, 5, 6, 18); inserearesta(&l, 6, 1, 8); inserearesta(&l, 6, 5, 18); inserearesta(&l, 6, 7, 9); inserearesta(&l, 6, 8, 9); inserearesta(&l, 7, 6, 9); inserearesta(&l, 7, 8, 3);
    inserearesta(&l, 7, 9, 6); inserearesta(&l, 8, 1, 9); inserearesta(&l, 8, 6, 9); inserearesta(&l, 8, 7, 3); inserearesta(&l, 8, 9, 4); inserearesta(&l, 8, 10, 2); inserearesta(&l, 9, 2, 9);
    inserearesta(&l, 9, 7, 6); inserearesta(&l, 9, 8, 4); inserearesta(&l, 9, 10, 2); inserearesta(&l, 10, 1, 8); inserearesta(&l, 10, 2, 9); inserearesta(&l, 10, 8, 2); inserearesta(&l, 10, 9, 2);

    do{
        printf("Comando -> MENU\n");
        printf("1 -> Inserir vertice\n2 -> Inserir aresta\n3 -> Visualizar Grafo\n4 -> Remover vertice\n5 -> Remover aresta\n6 -> Informar grau de um vertice\n7 -> Informar se o grafo e conexo\n");
        printf("8 -> Converter para matriz\n9 -> Caminhamento em amplitude\n10 -> Caminhamento em profundidade\n11 -> Caminho minimo\n12 -> Arvore geradora minima\n13 -> Sair\nDigite o comando: ");
        scanf("%d", &comando);
        printf("\n");
        if(comando==1)
        {
            printf("Informe o Vertice: ");
            scanf("%d", &x);
            inserefim(&l,x);
            printf("\n");
        }
        if(comando==2)
        {
            printf("Informe a aresta 1: ");
            scanf("%d", &a1);
            printf("Informe a aresta 2: ");
            scanf("%d", &a2);
            printf("Informe o peso da aresta: ");
            scanf("%d", &p);
            if(verifica(&l, a1, a2)==TRUE)
            {
                inserearesta(&l, a1, a2, p);
                inserearesta(&l, a2, a1, p);
            }
            printf("\n");
        }
        if(comando==3)
        {
            imprimelista(&l);
            printf("\n");
        }
        if(comando==4)
        {
            int z;
            printf("Informe o Vertice a ser removido: ");
            scanf("%d", &x);
            z=removetodasaresta(&l, x);
            removevert(&l,z);
            removearestas(&l, x);
            printf("\n");
        }
        if(comando==5)
        {
            printf("Informe o primeiro vertice: ");
            scanf("%d", &a1);
            printf("Informe o segundo vertice: ");
            scanf("%d", &a2);
            removeumaaresta(&l, a1, a2);
            removeumaaresta(&l, a2, a1);
            printf("\n");
        }
        if(comando==6)
        {
            printf("Informe o vertice que se deseja saber o grau: ");
            scanf("%d", &x);
            y=informagrau(&l, x);
            if(y==-1)
                printf("Vertice inexistente\n\n");
            else
                printf("Grau = %d\n\n",y);
        }
        if(comando==7)
        {
            x=conexo(&l);
            if(x==1)
                printf("Grafo Conexo\n\n");
            else
                printf("Grafo nao conexo\n\n");
        }
        if(comando==8)
        {
            y=tamanho(&l);
            if(y==0)
            {
                printf("Grafo Vazio\n\n");
            }
            else
            {
                matriz(&l);
            }
        }
        if(comando==9)
        {
            y=tamanho(&l);
            if(y==0)
            {
                printf("Grafo Vazio\n\n");
            }
            else
            {
                printf("Informe o vertice de origem para a busca: ");
                scanf("%d", &x);
                j=existe(&l, x);
                if(j==0)
                    printf("Verrice nao existe\n\n");
                else
                    buscalargura(&l, x);
            }
        }
        if(comando==10)
        {
            y=tamanho(&l);
            if(y==0)
            {
                printf("Grafo Vazio\n\n");
            }
            else
            {
                printf("Informe o vertice de origem da busca: ");
                scanf("%d", &x);
                j=existe(&l, x);
                if(j==0)
                    printf("Verrice nao existe\n\n");
                else
                    buscaprofundidade(&l, x);
            }
        }
        if(comando==11)
        {
            y=tamanho(&l);
            if(y==0)
            {
                printf("Grafo Vazio\n\n");
            }
            else
            {
                printf("Informe o vertice de origem da busca: ");
                scanf("%d", &x);
                j=existe(&l, x);
                if(j==0)
                    printf("Verrice nao existe\n\n");
                else
                    caminhominimoprim(&l, x);
            }
        }
        if(comando==12)
        {
            y=tamanho(&l);
            if(y==0)
            {
                printf("Grafo Vazio\n\n");
            }
            else
            {
                printf("Informe o vertice de origem da busca: ");
                scanf("%d", &x);
                j=existe(&l, x);
                if(j==0)
                    printf("Verrice nao existe\n\n");
                else
                caminhominimodijkstra(&l, x);
            }
        }
        if(comando==13)
        {
        encerrar(&l);
        }
    }while(comando!=13);

return 0;
}
